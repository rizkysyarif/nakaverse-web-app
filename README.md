# Nakaverse Web App

> Simple Landing Page Cloathing Brand
 
## Table of contents

> * [Nakaverse Web App](#nakaverse-web-app)
>   * [Table of contents](#table-of-contents)
>   * [Introduction](#introduction)
>   * [Deployment](#deployment)
>   * [Installation](#installation)
>     * [Clone](#clone)
>   * [License](#license)

## Introduction

> This is simple landing page cloathing brand.

## Deployment

> Live preview in https://startship-app.netlify.app/

## Installation

### Clone
```
$ git clone https://gitlab.com/rizkysyarif/nakaverse-web-app.git
$ cd nakaverse-web-app
```

## License
MIT

